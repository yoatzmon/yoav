const test = require('./controllers/test')
const ques = require('./controllers/question')
const user = require('./controllers/user')
const dash = require('./controllers/dash')
const test_details = require('./controllers/test_details')
const globalTest = require('./controllers/global_test')
const globalTestDetails = require('./controllers/global_test_details')
const globalQuestion = require('./controllers/global_question')


module.exports = function Router(app) {

  // -------------------get--------------------------

  //use
  app.get('/testsdetails', async function (req, res) {
    const list = await test_details.read({ test_id: req.query.id })
    res.send(list)
  })


  //use
  app.get('/usertest', async function (req, res) {
    const token = req.headers.authorization
    const list = await globalTest.readTest(req.query.id, token)
    res.send(list)
  })


  //use
  app.get('/test', async function (req, res) {
    const list = await test.readOne({ _id: req.query.id })
    console.log(list);
    res.send(list)
  })

  //use
  app.get('/publishtest', async function (req, res) {
    try {
      const token = req.headers.authorization
      console.log(`publishToken : ${token}`)
      const list = await globalTest.publishTest(req.query.id, token)
      // console.log(list);
      res.send(list)
    }
    catch (error) {
      console.log(error)
      res.status(400).send({ error: error.massage || error });
    }
  })

  //use
  app.get('/testdetails', async function (req, res) {
    const token = req.headers.authorization
    const list = await globalTestDetails.endTest(req.query.id, token)
    console.log(list);
    res.send(list)


  })

  //use
  app.get('/createquestion', async function (req, res) {
    const token = req.headers.authorization
    // console.log(`servertoken : ${token}`)
    const list = await globalQuestion.createQues(req.query.id, token)
    res.send(list)
  })

  //use
  app.get('/question', async function (req, res) {
    const token = req.headers.authorization
    console.log(`servertoken : ${token}`)
    const list = await ques.read(req.query.id, token)
    res.send(list)
  })

  //use
  app.get('/getanswer', async function (req, res) {
    const list = await ques.readOne({ _id: req.query.id })
    res.send(list)
  })


  //use
  app.get('/dash', async function (req, res) {
    // const token = req.headers.authorization
    const list = await dash.dash(req.query.id)
    res.send(list)

  })

  //use
  app.get('/mytest', async function (req, res) {
    // const token = req.headers.authorization
    const list = await dash.myTest(req.query.id)
    res.send(list)

  })

  //use
  app.get('/testdone', async function (req, res) {
    // const token = req.headers.authorization
    const list = await dash.testDone(req.query.id)
    res.send(list)

  })

  // --------------------post---------------------

  //use
  app.post('/user', async function (req, res) {
    console.log('yossef')
    const token = req.headers.authorization
    const list = await user.getUser(token)
    console.log(list);
    res.send(list)
    console.log('yoav')

  })


  //use
  app.post('/register', async function (req, res) {
    try {
      const list = await user.register(req.body)
      // console.log(list);
      res.send(list)
    }
    catch (error) {
      console.log(error)
      res.status(400).send({ error: error.massage || error });
    }

  })

  //use
  app.post('/login', async function (req, res) {
    try {
      const { email, password } = req.body
      const list = await user.login(email, password)
      res.send(list)
    }
    catch (error) {
      console.log(error)
      res.status(400).send({ error: error.massage || error });
    }

  })


  //use
  app.post('/makesketch', async function (req, res) {
    try {
      const token = req.headers.authorization
      const lis = await globalTestDetails.endTest(req.query.id, req.body, token)
      res.send(lis)
    }
    catch (e) {
      res.status(400).send(e)
    }
  })

  //use
  app.post('/createtest', async function (req, res) {
    try {
      const token = req.headers.authorization
      // console.log(`servertoken : ${token}`)
      const list = await test.create(req.body, token)
      res.send(list)
    }
    catch (e) {
      res.status(400).send(e)
    }
  })

  //use
  app.post('/updatetest', async function (req, res) {
    try {
      const token = req.headers.authorization
      // console.log(`servertoken : ${token}`)
      const list = await test.update(req.query.id, req.body, token)
      res.send(list)
    }
    catch (e) {
      res.status(400).send(e)
    }
  })

  // ------------------put---------------------------------

  //use
  app.put('/unactive', async function (req, res) {
    try {
      const list = await test.unActive(req.query.id)
      res.send(list)
    }
    catch (e) {
      res.status(400).send(e)
    }
  })

  //use
  app.put('/updatequestion', async function (req, res) {
    try {
      const list = await ques.update(req.query.id, req.body)
      res.send(list)
    }
    catch (error) {
      console.log(error)
      res.status(400).send({ error: error.massage || error });
    }

  })



  // --------------------delet--------------------------------

  //use
  app.delete('/deletquestion', async function (req, res) {
    const list = await ques.delet(req.query.id)
    res.send(list)
  })

}
