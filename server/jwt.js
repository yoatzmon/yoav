const jwt = require('jsonwebtoken');
const User = require('./controllers/user');
require('dotenv').config()
process.env.SECRET_TOKEN
// const secret = '3245n235n44kj'//some key to open the token. shold be in .env file

exports.createToken = (id)=>{
    const token = jwt.sign({id},process.env.SECRET_TOKEN,{expiresIn:'10d'})
    // console.log(token)
    return token
}

exports.verifyToken = async (token)=>{
    
    if(!token) throw {err : `token is required!`}
    const tokenData = jwt.verify(token, process.env.SECRET_TOKEN) || {}
    // console.log(tokenData)
    if( Date.now > tokenData.exp * 1000) 
    throw {error:'token unouthrizing'}


    return await User.readOne ({_id : tokenData.id}) 
}