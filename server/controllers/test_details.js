const test_details = require('../models/test_details')
// const bcryptjs  = require('bcryptjs')


async function create(body) {

    return await test_details.create(body)
}

exports.create = create

async function read(filter) {
    return await test_details.find(filter)
}
exports.read = read

async function readOne(filter) {

    return await test_details.findOne(filter)
}
exports.readOne = readOne

async function update(_id, newData) {

    return await test_details.findByIdAndUpdate(_id, newData, { new: true })
}
exports.update = update
