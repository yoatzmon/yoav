const User = require('./user')
const Test = require('./test')
const Ques = require('./question')
const Details = require('./test_details')
const jwt = require('jsonwebtoken');
require('dotenv').config()
process.env.SECRET

// manage token situation
async function checkToken(token) {
    try {
        if (!token) throw { error: 'Token is required' }
        let verToken = jwt.verify(token, process.env.SECRET_TOKEN)
        if (!verToken) throw { error: 'Token is incorrect' }

        // Convert the token to id:
        return verToken.id
    }
    catch (err) {
        console.log(err)
    }
}
exports.checkToken = checkToken


//פונקציה שמחשבת את סכום הציונים
async function calc(id) {

    let counter = 0
    const ques = await Ques.read(id)
    // console.log(` ques : ${ques}`)
    ques.forEach(item => counter = counter + item.score)
    if (counter != 100)
        throw `the test grade must be 100!`
    else {
        // console.log('100!')
        return true
    }


}
exports.calc = calc

//פונקציה שבודקת מיילים
async function mailCheck(id) {

    let sum = 0
    const test_mail = await Test.readOne({ _id: id })
    // console.log(` testMail : ${test_mail}`)
    const user_mail = await User.read()
    // console.log(` user_mail : ${user_mail}`)
    for (let mail of test_mail.mailing_list) {
        for (let m of user_mail) {
            if (mail == m.email) {
                sum++
            }
        }
    }
    if (sum != test_mail.mailing_list.length)
        throw ('mailing list not ligal')
    else {
        // console.log(sum)
        return true
    }


}
exports.mailCheck = mailCheck

//כשהמשתמש לוחץ על מבחן
async function readTest(id, token) {


    const creater_id = await checkToken(token)
    const user = await User.read({ _id: creater_id })
    if (!user) throw { error: 'user does not exist!!!' }

    const q = await Ques.read(id, token)
    const tes = await Test.readOne({ _id: id })
    const testAndQues = [tes, q]
    return testAndQues
}
exports.readTest = readTest

//  פונקציית פרסום מבחן
//  1.בדיקה שכל השאלות ביחד מקנות ציון של 100
// 2.בדיקה שכל המיילים שנמצאים ברשימת אכן קיימים
//3.יצירת טסט דיטלס חדש עם פרטי המבחן והמשתמש 
async function publishTest(id, token) {


    const creater_id = await Test.checkToken(token)
    const user = await User.read({ _id: creater_id })
    if (!user) throw 'user does not exist!!!'

    if (await calc(id) && await mailCheck(id)) {

        const test_mail = await Test.readOne({ _id: id })
        // console.log(` hey ya : ${test_mail.submission_date}`)

        const user_mail = await User.read()
        // console.log(user_mail)
        const creater = await User.readOne({ _id: test_mail.creater_id })



        for (let mail of test_mail.mailing_list) {
            for (let m of user_mail) {
                if (mail == m.email) {
                    let test_details = {
                        user_id: m._id,
                        test_id: test_mail._id,
                        user_test: m._id + test_mail._id,
                        status: "ready",
                        submission_date: test_mail.submission_date,
                        test_name: test_mail.name,
                        tester_name: creater.full_name
                    }
                    await Details.create(test_details)
                }
            }
        }
    }
    else
        throw 'test is not ready yet'

}
exports.publishTest = publishTest
