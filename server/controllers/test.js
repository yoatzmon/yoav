const test = require('../models/test')
const User = require('./user')
const jwt = require('jsonwebtoken');
require('dotenv').config()
// const bcryptjs  = require('bcryptjs')

async function checkToken(token) {
    try {
        console.log(`function token ${token}`)
        if (!token) throw { error: 'Token is required' }
        let verToken = jwt.verify(token,process.env.SECRET_TOKEN)
        if (!verToken) throw { error: 'Token is incorrect' }

        // Convert the token to id:
        return verToken.id
    }
    catch(err) {
        console.log(err)
    }
}
exports.checkToken = checkToken

async function create(body,token) {

    const creater_id = await checkToken(token)
    const user = await User.readOne({ _id: creater_id })
    if (!user) throw 'user does not exist!!!' 


    let now = new Date(body.submission_date)
    let milisecond = now.getTime()
   
    if(Date.now() > milisecond){
        throw `date is not reasnable`
    }

    body.creater_id = creater_id

    return await test.create(body)
}
exports.create = create

async function read(filter) {
    return await test.find(filter)
}
exports.read = read

async function readOne(filter) {
    return await test.findOne(filter)
}
exports.readOne = readOne



async function update(_id, newData,token) {

    console.log(`token ${token}`)
    const creater_id = await checkToken(token)
    const user = await User.read({ _id: creater_id })
    if (!user) throw { error: 'user does not exist!!!' }

    const tes = await readOne({_id:_id})
    
    let now = new Date(newData.submission_date)
    let milisecond = now.getTime()
    if(Date.now() > milisecond){
        console.log('haaaaaaaaa')
        throw `date is not reasnable`
    }

    tes.name = newData.name
    tes.discription = newData.discription
    tes.form_type = newData.form_type
    tes.mailing_list = newData
    newData.length == undefined ? tes.status = 'Edit' : tes.status = 'Publish'
    tes.submission_date = newData.submission_date
//    console.log(`tes : ${tes}`)
    return await test.findByIdAndUpdate(_id, tes, { new: true })
}
exports.update = update



async function updateGrades(_id, newData) {

    const tes = await readOne({_id:_id})
    tes.students_grades.push({name : newData.name, grade:newData.grade})
    
    return await test.findByIdAndUpdate(_id, tes, { new: true })
}
exports.updateGrades = updateGrades



async function unActive(id){
    
    const unAct = await readOne({_id:id})

    if(unAct.status == 'Edit' )
        unAct.active = false 
    else
        throw `test is published you can't delete him now!`

    return await test.findByIdAndUpdate(id, unAct, { new: true })

}
exports.unActive = unActive
