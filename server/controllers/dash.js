const User = require('./user')
const TesT = require('./test')
const Details = require('./test_details')
const Test = require('./global_test')
const jwt = require('jsonwebtoken');
require('dotenv').config()
process.env.SECRET

async function dash(id) {

    const testIcreate = await TesT.read({ creater_id: id })

    const idies = testIcreate.map(item => item._id)
    // console.log(idies)

    const detail = await Details.read({ test_id: { $in: idies } })
    // console.log(`details ${detail}`)
    const dID = detail.map(item => item.user_id)
    const users = await User.read({ _id: { $in: dID } })
    // console.log(`users : ${users}`)
    for (let test of testIcreate) {
        for (let d of detail) {
            for (let u of users) {
                if (test._id == d.test_id && d.user_id == u._id && d.status == 'done') {
                    test.students_grades.push({ name: u.full_name, grade: d.grade })
                    // console.log(test.students_grades)
                }
                // else
                // console.log('no grades yet!')
            }
        }
        await TesT.updateGrades(test._id, test.students_grades)
    }

    return testIcreate
}
exports.dash = dash

async function myTest(id) {

    const my = await Details.read({ $and: [{ user_id: id }, { status: "ready" }] })

    return my
}
exports.myTest = myTest

async function testDone(id) {

    const my = await Details.read({ $and: [{ user_id: id }, { status: "done" }] })

    return my
}
exports.testDone = testDone