const Ques = require('./question')
const Test = require('./global_test')
const TesT = require('./test')
const User = require('./user')
const jwt = require('jsonwebtoken');
require('dotenv').config()
process.env.SECRET

async function createQues(id, token) {

    const creater_id = await Test.checkToken(token)
    const user = await User.readOne({ _id: creater_id })
    if (!user) throw { error: 'user does not exist!!!' }

    const test = await TesT.readOne({ _id: id })

    let question = {

        test_id: test._id,
        answers: [{ discription: '', currect: false }]
    }


    return await Ques.create(question)
}
exports.createQues = createQues

