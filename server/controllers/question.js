const Ques = require('../models/question')
const User = require('./user')
const Test = require('./global_test')
const jwt = require('jsonwebtoken');
const { default: axios } = require('axios');
require('dotenv').config()


async function create(body) {

    return await Ques.create(body)
}
exports.create = create


async function read(id, token) {

    const creater_id = await Test.checkToken(token)
    const user = await User.read({ _id: creater_id })
    if (!user) throw { error: 'user does not exist!!!' }

    return await Ques.find({ test_id: id })
}
exports.read = read

async function readOne(filter, projection) {
    return await Ques.findOne(filter, projection)
}
exports.readOne = readOne

async function update(_id, newData) {
    let counter = 0
    newData.answers.forEach(item => counter = counter + Number(item.score))
    // console.log(`counter : ${counter}`)
    if (newData.score != counter)
        throw (`answer score not currect`)
    else if (newData.answers.find(item => item.discription == ''))
        throw (`you missing answer discription!`)

    return await Ques.findByIdAndUpdate(_id, newData, { new: true })

}
exports.update = update

async function delet(_id) {

    return await Ques.findByIdAndDelete(_id)

}
exports.delet = delet