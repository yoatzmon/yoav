const Details = require('./test_details')
const Test = require('./global_test')
const Ques = require('./question')
const User = require('./user')
const jwt = require('jsonwebtoken');
require('dotenv').config()
process.env.SECRET

// פונקציה שברגע שהנבחן לוחץ על שלח בחינה
//1. מכניסה למערך של סקיצה את הניקוד של כל שאלה ותשובה שהמשתמש הזין 
//2.הופכת את הסטטוס של המבחן לדאן
async function endTest(id, data, token) {

    let grade = 0

    const creater_id = await Test.checkToken(token)
    const user = await User.read({ _id: creater_id })
    if (!user) throw { error: 'user does not exist!!!' }

    // console.log(`id : ${id}`)
    const end = await Details.readOne({ $and: [{ user_id: creater_id }, { test_id: id }] })


    let now = new Date(end.submission_date)
    let milisecond = now.getTime()
    if (Date.now() > milisecond) {
        throw `test submission time has passed`
    }

    end.sketch = data
    end.sketch.forEach(item => grade = grade + Number(item.answer_score))
    end.grade = grade
    end.status = 'done'

    return await Details.update(end._id, end)

}
exports.endTest = endTest
