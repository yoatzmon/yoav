const User = require('../models/user')
const Test = require('./global_test')
const bcryptjs = require('bcryptjs')
const { createToken, verifyToken } = require('../jwt')
require('dotenv').config()
process.env.SECRET

async function create(body) {

    return await User.create(body)
}
exports.create = create

async function read(filter) {

    return User.find(filter)

}
exports.read = read

async function readOne(filter, projection) {
    return await User.findOne(filter, projection)
}
exports.readOne = readOne

async function update(_id, newData) {
    return await User.findByIdAndUpdate(_id, newData, { new: true })
}
exports.update = update

async function getUser(token) {

    const user = await Test.checkToken(token)
    return await readOne({ _id: user })
}
exports.getUser = getUser

exports.register = async function (data) {

    const pass = data.password

    data.password = bcryptjs.hashSync(data.password)

    await create(data)

    return await login(data.email, pass)

}

async function login(email, password) {

    const user = await readOne({ email }, '+password')
    if (!user) throw 'Faild to login'


    if (!bcryptjs.compareSync(password, user.password))
        throw 'password wrong'

    const token = createToken(user._id)

    user.token = token

    // User.last_seen = Date.now()

    const updatedUser = await update(user._id, user)

    updatedUser.token = token

    return updatedUser

}
exports.login = login
