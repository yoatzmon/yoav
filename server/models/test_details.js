const mongoose = require('mongoose');
const { Schema, model } = mongoose

const testDetailsSchema = new Schema({

    user_id: {
        type: String,
        required: true
    },
    test_id: {
        type: String,
        required: true
    },
    user_test:{
        type: String,
        required:true,
        unique: true
    },
    status: {
        type: String,
        enum: ["ready", "done"],
        default: "ready"
    },
    grade: {
        type: Number,
        
    },
    submission_date: String,
    test_name : String,
    tester_name: String,
    sketch:
        [
            { 
                qua_score:  String,
                answer_score:  String,
                question_id: String,
                answer : String
            }
        ]
})

const testDetailsModel = model('test_details', testDetailsSchema)

module.exports = testDetailsModel // == export deafult