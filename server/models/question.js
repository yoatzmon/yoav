const mongoose = require('mongoose');
const { Schema, model } = mongoose

const qaSchema = new Schema({
    Instructions: {
        type: String,
        
    },
    name: {
        type: String,
        
    },
    test_id: {
        type: String,
        required: true
    },
    ques_type: {
        type: String,
        enum: ["radio", "checkbox"],
        default: "radio"
    },
    score:  Number,
    answers: [{
        discription: { type: String, default : "discription" },
        score: { type: Number, default : 0 }
        // currect: { type: Boolean}
    }],
    the_answer : String
})

const qaModel = model('question', qaSchema)

module.exports = qaModel // == export deafult