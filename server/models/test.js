const mongoose = require('mongoose');
const { Schema, model } = mongoose

const testSchema = new Schema({

    discription: {
        type: String,
        required:true
    },
    name: {
        type: String,
        required:true
    },
    create_date: {
        type: Date
    },
    expire_date: {
        type: Date

    },
    creater_id: {
        type: String,
        require: true
    },
    form_type: {
        type: String,
        enum: ["Test", "Survey"],
        default: "Test"
    },
    submission_date: String,
    status: {
        type: String,
        enum: ["Edit","Publish"],
        default: "Edit"
    },
    active: {
        type: Boolean,
        default:true
    },
    students_grades : [{
        name :String,
        grade : Number
    }],
    mailing_list: Array
})

const testModel = model('test', testSchema)

module.exports = testModel // == export deafult