const mongoose = require('mongoose');
const { Schema, model } = mongoose

const userSchema = new Schema({
        full_name: {
                type: String,
                required: true,
                unique: true
        },
        email: {
                type: String,
                unique: true,
                required: true
        },
        password: {
                type: String,
                select: false,
                required: true,
                unique: true
        },
        isActive: {
                type: Boolean,
                default: true
        },
        token: {type:String,
                select:false},
})

const userModel = model('user', userSchema)

module.exports = userModel // == export deafult