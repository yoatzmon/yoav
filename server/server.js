const express = require('express')
const app = express()
const cors = require('cors')
const db = require('./db')
app.use(cors())
app.use(express.json())
app.use(express.static('build'))
require('dotenv').config()

const Router =  require('./router')

db.connect()
    .then(() => {
        Router(app)
        
        app.listen(process.env.PORT || 4000, () =>
            console.log('server up'))
    })
