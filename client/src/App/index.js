import { Route, Switch, Link, useHistory } from "react-router-dom"
import DashBord from './DashBord'
import DashTest from './DashTest'
import TestForm from './TestForm'
import UserTest from './UserTest'
import ReadTest from './ReadTest'
import { useContext, useState } from "react";
import { LoginContext } from '../Login'
import './App.css'

function App() {

    const History = useHistory(),
        [user, setUser] = useContext(LoginContext)

    //על ידי לחיצה על לוגאאוט הפונקציה מוחקת את מה שנמצא בלוקאל סטורג' תחת הכותרת טוקן
    //על מנת שנשוב אל דף הלוג אין
    function logout() {
        delete localStorage.token
        setUser()
        History.push('/')
    }

    return <div className="App">
        <div className="header">
            <img className="logo" src="https://img.icons8.com/dotty/80/000000/face-id.png" />

            {user ? <h3 className="hey"> {user.full_name}
                <button className="butLogout" onClick={logout}>log out</button>
            </h3> : ''}


            <div className="navigation">
                <Link to='/' className="manu">home</Link>
                <div>|</div>
                <Link to='/my-tests' className="manu"> mytest</Link>
            </div>
            <div className="contact">
            <div>
                <h2 className="look">look for us</h2>
            </div>
            
            <div className="flex">
                <div className="icon"><img src="https://img.icons8.com/color/48/000000/whatsapp.png"/></div>
                <div className="icon"><img src="https://img.icons8.com/color/48/000000/facebook.png"/></div>
                <div className="icon"><img src="https://img.icons8.com/color/48/000000/gmail-new.png"/></div>
            </div>
            
            </div>
        </div>

        <Switch>
            <div className="backroundPicture">
                <Route path='/' component={DashBord} exact></Route>{/* create tests*/}
                <Route path='/my-tests' component={DashTest} ></Route>{/* assigned tests */}
                <Route path='/test/:test' component={UserTest} ></Route>
                <Route path='/test-form/:testId?' component={TestForm} ></Route>
                <Route path='/readtest/:test' component={ReadTest} ></Route>
            </div>
        </Switch>

    </div>
}

export default App
