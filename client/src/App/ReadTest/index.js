import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { SERVER_URL } from "../../globals"
import Loader from "../Loader"
export default function ReadTest() {

    const [userTest, setUserTest] = useState(''),
    { test } = useParams(),
    [tes, setTes] = useState(test),
    [error, setError] = useState(''),
    [mail, setMails] = useState([{}]),
    [send, setSend] = useState('')


    //פונקציה שמקבלת את פרטי המבחן והשאלות
    async function getTest() {
        const readTest = await axios.get(`${SERVER_URL}/usertest?id=${test}`, { headers: { authorization: localStorage.token || sessionStorage.token } })

        setUserTest(readTest.data)

    }

    //כשיוצר המבחן מוסיף מייל ושולח אותו הפונקציה הנוכחית שולחת את המייל/ים לבדיקה
    async function mails(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )


        await updateMails(values)

    }

    //פונקציה ששולחת לשרת את רשימת המיילים לבדיקה
    //ומעדכנת את פרטי המבחן שנוצר
    async function updateMails(values) {
        try {
            const num = Object.keys(values).length

            const arr = [...Array(num)]

            const newArr = arr.map((i, index) => {
                return values[index]
            })



            await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, newArr, { headers: { authorization: localStorage.token || sessionStorage.token } })


            await axios.get(`${SERVER_URL}/publishtest?id=${tes}`, { headers: { authorization: localStorage.token || sessionStorage.token } })


            await setSend('The test was sent successfully!')

        }
        catch (e) {
            setError(e.response.data.error.name ? `you can't send the test twice to the same person` : e.response.data.error)
        }

    }

    //הוספת מייל
    async function addMail(e) {
        e.preventDefault()
        setMails([...mail, {}])
    }

    useEffect(getTest, [])

    return userTest ?
        <div className="doneTest">
            <div>
                <h1 className="titles"> {userTest[0].name} </h1>
                <h3 className="titles"> {userTest[0].discription}</h3>
            </div>
            <form>
                {userTest[1].map(item =>
                    <div className="question">
                        <h3 className="titles">{item.name}</h3>
                        <h4 className="titles">{item.Instructions}</h4>
                        <div className="answers">
                            {item.answers.map(ans =>
                                <label className="answer">
                                    {ans.discription} {`    `}
                                    <input
                                        type={item.ques_type}
                                        name={ans}
                                    />
                                </label>

                            )}
                        </div>
                    </div>
                )}
                <hr></hr>
            </form>

            <form className="mailing" onSubmit={mails}>
                {mail.map((item, index) =>

                    <div>
                        <div className="mails">
                            <input className="testInput" type="text" placeholder='add student email' name={index} />
                            <button className="newTest" onClick={addMail}>add email</button>
                        </div>
                    </div>
                )}
                {!send ? <input className="newTest" type="submit" value='send test' /> : ''}
                <div className="err">{send}</div>
                <div className="err">{error}</div>
            </form>
        </div> : <Loader/>
}