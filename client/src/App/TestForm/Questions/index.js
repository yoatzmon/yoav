import axios from "axios"
import { createContext, useContext, useEffect, useState } from "react"
import { DeletContext } from ".."
// import Answer from './Answer'
import { SERVER_URL } from "../../../globals"

export default function Questions({ param }) {

    const [answersList, setAnswersList] = useState([]),
        [questionData, setQuestionData] = useState(''),
        [del, setDel] = useContext(DeletContext),
        [error, setError] = useState(''),
        [ques, setSaveQues] = useState('')


    //פונקציה שמקבלת את האובייקט של השאלה מהדאטא בייס ומתאחלת סטייטים 
    async function getAnswers() {

        const res = await axios.get(`${SERVER_URL}/getanswer?id=${param}`)

        setQuestionData(res.data)
        setAnswersList(res.data.answers)
    }

    //פונקציה שמוסיפה אובייקט לסטייס של התשובות בכל פעם שהמשתמש לוחץ על הוסף שאלה
    function addAnswer(e) {
        e.preventDefault()

        setAnswersList([...answersList, { discription: "", score: '' }])
    }

    //פונקציה שמסירה אובייקט מהסטייט של התשובות בכל פעם שהמשתמש מסיר תשובה
    function removeAnswer(e, index) {
        e.preventDefault()
        const rlrlr = [...answersList]
        if (rlrlr.length != 1)
            rlrlr.splice(index)
        setAnswersList(rlrlr)
    }

    //פונקציה שמופעלת כאשר המשתמש לוחץ על שמור שאלה
    async function saveQuestion(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        await updateQuestion(values)
    }

    //פונקציה ששולחת לסרבר את העדכון של השאלה והתשובות
    async function updateQuestion(values) {
        try {
            const num = Object.keys(values).length - 4

            const arr = [...Array(num)]

            const newArr = arr.map((i, index) => {
                return {
                    discription: values[index],
                    score: values[index + 10]
                }
            })

            newArr.splice(newArr.length / 2, newArr.length / 2)

            const list = {

                Instructions: values.Instructions,

                name: values.name,

                ques_type: values.ques_type,

                score: values.score,

                answers:

                    [...newArr]

            }
            console.log(list);

            const res = await axios.put(`${SERVER_URL}/updatequestion?id=${param}`, list)

            setError('')
            setSaveQues('question saved!')
        }
        catch (e) {
            setSaveQues('')
            setError(e.response.data.error)
        }

    }

    //פונקציה שמוחקת ששולחת לסרבר שאלה למחיקה
    async function deletQuestion(e) {
        e.preventDefault()
        console.log(param)
        await axios.delete(`${SERVER_URL}/deletquestion?id=${param}`)
        setDel([...del, {}])

    }

    //רהצגת השאלות בכל פעם שהדף מתרנדר
    useEffect(getAnswers, [])

    return <div className='new_question'>
        <form onSubmit={saveQuestion}>
            <input className="questionInput" id="name" type="text" placeholder="enter the question" required name="name" defaultValue={questionData ? questionData.name : ''} /><br></br>
            <input className="questionInput" id="Instructions" type="text" placeholder="enter the Instructions" required name="Instructions" defaultValue={questionData ? questionData.Instructions : ''} /><br></br>
            <input className="questionInput" id="score" type="text" placeholder="enter the score" required name="score" defaultValue={questionData ? questionData.score : ''} /><br></br>
            <select className="questionInput" name="ques_type" id="ques_type" defaultValue={questionData ? questionData.ques_type : ''}>
                <option value="radio">radio</option>
                <option value="checkbox">checkbox</option>
            </select><br></br>
            {
                answersList.map((item, index) =>
                    <div>
                        <input className="answerInput" type='text' placeholder='enter answer' defaultValue={item.discription} name={index} />
                        <input className="answerInput" type='score' placeholder='enter score' defaultValue={item.score} name={'1' + String(index)} />
                        <button className="newTest" onClick={(e) => addAnswer(e)}>+</button>
                        <button className="newTest" onClick={(e) => removeAnswer(e, index)}>-</button></div>
                )}
            <div className="addRemove">
                <button className="addRemoveButtun" onClick={deletQuestion}>delete question</button>
                <input className="addRemoveButtun" type="submit" value="save question" /><br></br>
            </div>
            <div className="notErr" >{ques}</div>
            <div className="err">{error}</div>
        </form>

    </div>
}