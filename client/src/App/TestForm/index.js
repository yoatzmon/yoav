import axios from "axios"
import Question from './Questions'
import { createContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { SERVER_URL } from "../../globals"
export const DeletContext = createContext()

export default function TestForm() {

    const { testId } = useParams(),
        delQues = useState(''),
        [tes, setTes] = useState(testId),
        [val, setVal] = useState(),
        [mail, setMails] = useState([{}]),
        [questionsList, setQuestionsList] = useState(''),
        [del, detDel] = delQues,
        [error, setError] = useState(''),
        [err, setErr] = useState(''),
        [send, setSend] = useState('')

    //פונקציה שמופעלת בלחיצה על טופס
    async function save(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        await newTest(values)
    }

    //כשהמשתמש לוחץ על שמור מבחן אם המבחן קיים הפונקציה שולחת את המבחן לעדכון
    //אחרת היא שולחת את הפרטים לייצור מבחן חדש
    async function newTest(values) {
        try {
            if (tes) {

                const ser = await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, values, { headers: { authorization: localStorage.token || sessionStorage.token } })
            }
            else {
                const res = await axios.post(`${SERVER_URL}/createtest`, values, { headers: { authorization: localStorage.token || sessionStorage.token } })
                console.log(res.data)
                setTes(res.data._id)
            }

            setErr('')
        }
        catch (e) {
            setErr(e.response.data)
        }
    }


    //הפונקציה שולחת את האי די של המבחן על מנת שהסרבר יצור שאלה חדשה בדאטא בייס
    async function createQuestions() {
        const new_question = await axios.get(`${SERVER_URL}/createquestion?id=${tes}`, { headers: { authorization: localStorage.token || sessionStorage.token } })
        console.log(new_question.data)
        await displayQuestions()
    }

    //פונקציה שמקבלת את כל השאלות שקשורות למבחן הנוכחי ומציגה אותן
    async function displayQuestions() {
        const question = await axios.get(`${SERVER_URL}/question?id=${tes}`, { headers: { authorization: localStorage.token || sessionStorage.token } })
        setQuestionsList(question.data)
    }


    //פונקציה שמוציאה מהטופס של המיילים את הפרטים שהמשתמש הזין
    async function mails(event) {
        event.preventDefault()
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )

        console.log(values)
        await updateMails(values)

    }

    //פונקציה ששולחת לסרבר את רשימת המיילים שהמשתמש הזין לבדיקה
    //וכמו כן מפרסמת את המבחן
    async function updateMails(values) {
        try {
            const num = Object.keys(values).length

            const arr = [...Array(num)]

            const newArr = arr.map((i, index) => {
                return values[index]
            })

            await axios.post(`${SERVER_URL}/updatetest?id=${tes}`, newArr, { headers: { authorization: localStorage.token || sessionStorage.token } })


            await axios.get(`${SERVER_URL}/publishtest?id=${tes}`, { headers: { authorization: localStorage.token || sessionStorage.token } })

            setError('')
            await setSend('The test was sent successfully!')

        }
        catch (e) {

            setError(e.response.data.error)
        }

    }

    //כשהמשתמש לוחץ על הוסף מייל הפונקציה מוסיפה לו שדה של מייל
    async function addMail(e) {
        e.preventDefault()
        setMails([...mail, {}])
    }



    //בכל פעם שהדף עולה או מתרנדר הפונקציה מקבלת את פרטי המבחן במידה והוא קיים
    useEffect(async () => {
        const value = await axios.get(`${SERVER_URL}/test?id=${testId}`)
        setVal(value.data)
    }, [])

    //הצגת השאלות כשהדף מתרנדר
    useEffect(displayQuestions, [del])

    return <div>
        <form className="makeTest" onSubmit={save}>

            <input className="testInput" type="text" name="name" placeholder="enter the test name" required defaultValue={val ? val.name : ''} /><br></br>
            <input className="testInput" type="text" name="discription" placeholder="enter the test discription" required defaultValue={val ? val.discription : ''} /><br></br>
            <label>DeAd LiNe</label><input className="testInput" type="date" name="submission_date" required defaultValue={val ? val.submission_date : ''} />
            <input className="newTest" type="submit" value="save test" />
            <div className="err">{err}</div>
        </form>
        <hr></hr>
        <div className="aligen">
            {tes && !err ? <button class="addQues" onClick={createQuestions}>add ques</button> : ''}
            <div className="all_ques">
                {tes ? (questionsList ?
                    questionsList.map(item =>

                        <DeletContext.Provider value={delQues}>
                            <Question param={item._id} />
                        </DeletContext.Provider>

                    )
                    : '')
                    : ''}
            </div>
            {questionsList.length > 0 ?
                <form className="mailing" onSubmit={mails}>
                    {mail.map((item, index) =>

                        <div>
                            <div className="mails">
                                <input className="testInput" type="text" placeholder='add student email' name={index} />
                                <button className="newTest" onClick={addMail}>add email</button>
                            </div>
                        </div>
                    )}


                    {!send ? <input className="newTest" type="submit" value='send test' /> : ''}
                    <div className="notErr">{send}</div>
                    <div className="err">{error}</div>
                </form> : ''}

        </div>
    </div>

}







