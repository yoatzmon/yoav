import { useEffect, useState } from "react";
import axios from "axios";
import Loader from "../Loader";
import { useContext } from "react";
import { LoginContext } from '../../Login'
import { Link } from "react-router-dom"
import { SERVER_URL } from "../../globals";
export default function DashBord() {


  const [dash, setDash] = useState(''),
    [del, setDel] = useState(''),
    [error, setError] = useState(''),
    user = useContext(LoginContext)



//פונקציה שמקבלת את המבחן שהמשתמש יצר ומכניסה אותם לתוך הסטייט
  async function fetchData() {

    const res = await axios.get(`${SERVER_URL}/dash?id=${user[0]._id}`)

    setDash(res.data)
  }

//פונקציה שמופעלת כאשר המשתמש לוחץ על מחק מבחן והיא מעדכנת בדאטא בייס את המבחן כלא אקטיבי
  async function unActive(e, item) {
    try {
      setError('')
      await axios.put(`${SERVER_URL}/unactive?id=${item._id}`)
      setDel([...del, {}])
    }
    catch (e) {
      // debugger
      setError(e.response.data)
    }
  }

//בכל פעם שהסטייס של דל משתנה מוצגים המבחנים שהמשתמש יצר
  useEffect(fetchData, [del])


  return dash ?
    <div className="testImade">
      <div className="title_middle">
        <h1 className="title">test i create</h1>
      </div>
      <hr></hr>
      <table>
        <tr>
          <th>Name</th>
          <th>discription</th>
          <th>status</th>
          <th>grades</th>
        </tr>
        {dash.map(item =>
        //רק במידה והמבחן פעיל בדאטא בייס הוא יוצג למשתמש
          item.active ?
            <tr>
              {item.status == 'Edit' ?

                //אם הסטטוס של המבחן במצב עריכה יוצר המבחן חוזר לדף העריכה
                <Link to={`/test-form/${item._id}`}>
                  
                  <td> {item.name}</td>
                  
                </Link>
                :
                //אם הסטטוס של המבחן הוא פאבליק אז בלחיצה יוצר המבחן יראה את המבחן שהוא יצר ללא אפשרות עריכה
                //, אך עם אפשרות לשליחה לתלמידים נוספים
                <Link to={`/readtest/${item._id}`}>
                  <td>{item.name}</td></Link>
              }
              <td>{item.discription}</td>
              <td>{item.status}</td>
              <td>
                {item.students_grades.map(i =>
                  i.name ?
                    <div> {i.name} - {i.grade}</div> : ''
                )}
              </td>
              <td className="deleteTest" onClick={(e) => unActive(e, item)}>delete test</td>


            </tr>
            : '')}

           {/* יצירת מבחן חדש */}
        <div>
          <Link to='/test-form'>
            <button class="newTest">+</button>
          </Link>
        </div>

        <div className="err">{error}</div>
      </table>
    </div >
    : <Loader/>
}